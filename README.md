Gbb Tagging CxAOD Code
======================

This is a superproject to organize the CxAOD code we use for the gbb
tagging calibration.


Quickstart
----------

Assuming you're on a machine with access to `/cvmfs/` you should be
able to run the following sequence to build the project:

```bash
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-boosted-hbb/gbb-calibration-superproject.git
cd gbb-calibration-superproject
source install.sh
```
This will compile the code and set up the correct environment. Note that the project is compiled with `-DCMAKE_BUILD_TYPE=Release` by default and in some cases it may be useful to recompile with `-DCMAKE_BUILD_TYPE=Debug` for debugging purposes.

To set up the environment for a previously compiled project do:

```bash
cd gbb-calibration-superproject
source setup.sh
```

Jobs are submitted through the executable `FlavourTaggingFramework` located in FrameworkExe\_FlavourTagging. A wrapper script is provided to for the gbb calibration that allows sets of jobs to be run easily. A simple test job can be run with:

```bash
submit_gbb.sh -n 10 /path/to/DAOD_directory | tee run.log
```

`submit_gbb.sh` Usage
----------

`submit_gbb.sh` will modify the configuration file `x86_64-centos7-gcc8-opt/data/FrameworkExe_FlavourTagging/framework-run-gbb.cfg` and then run

```bash
FlavourTaggingFramework -c framework-run-gbb.cfg -n 10 | tee run.log
```

To change configurations other than those exposed through `submit_gbb.sh` (e.g. to update tool configurations) it is necessary to modify this file directly.

To launch a full grid production, create a git tag and make sure that the current commit passes the git CI. Then run

```bash
voms-proxy-init -voms atlas
submit_gbb.sh all
```

The submission options are:
```
usage: submit_gbb.sh [-h|--help] [--noSys] [--noTrkFilter] [--noMuonReq] [--noPresel] [--minimal] [-n|--maxEvents <n>] [--resubmit] -- <sample> [<sample2> ...]
         -h|--help      Display this help message
         --noSys        Set nominal-only flag for CxAODMaker and TupleMaker
         --noTrkFilter  Set flag to remove track slimming in TupleMaker.
                        This greatly increases ntuple size.
         --noPresel     Set flag to remove preselection. For selection studies.
                        This greatly increases ntuple size.
         --noMuonReq    Set flag to remove muon requirement in preselection.
                        This greatly increases ntuple size.
         --minimal      Remove some branches, including all track branches.
                        This greatly reduces ntuple size.
         -n|--maxEvents Process n events per file
         --resubmit     Set flag to overwrite job with same output dataset name.
                        Useful for resubmitting broken jobs.
                        Sample(s) to run are taken from the positional arguments.
                        These can either be the path to a directory containing
                        ROOT files, the path to a list of files or one of the
                        following special cases:
                          all, all_data, all_mc, incl_mc, mufilt_mc.
```

For studies of the event selection or track variables, enable the `--noPresel` and `--noTrkFilter` options. Be aware, though, that these will greatly increase the output file size and should not be used for productions with systematics.

It is recommended to use the `--minimal` options when running with systematics to keep file sizes manageable. Note that systematics are stored in separate trees so file size is approximately (size of nominal tree)\*(number of systematics).

## Updating the package

In order to submit jobs to the grid this project must be on a git tag with no uncommitted code. `gbb-calibration-superproject` only stores a set of commit numbers for the submodules, so there are two steps to creating a new commit.
First, make your changes to the submodule(s) and merge them to the master branch(es). Then make a commit in the superproject with the new commit numbers of the submodules. Lastly, if you want to do a grid production, make an annotated tag with `git tag -a <tag_name>` where `<tag_name>` must start with the AnalysisBase release number of the project (e.g. `21.2.108_15DEC`). Please write a useful tag message including a broad overview of changes since the latest tag and/or the purpose of the new production.
