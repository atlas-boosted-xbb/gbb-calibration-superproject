#!/usr/bin/env bash 
if [ "$1" = "-h" ] || [ "$1" = "--help" ] ; then
    echo 
    echo "Copy data binary files from CERN afs"
    echo "Usage: $0 [option...]" 
    echo
    echo "   no option (default)        Copy from main directory /afs/cern.ch/work/v/vhbbframework/public/data/dataForCxAODFramework_190601"
    echo "   [directory]                Copy from user dirfined directory "  
    echo "   -h, --help                 this help "
    echo
    kill -INT $$
fi

# see if directory provided else take core packages as default
if [ "$1" != "" ]  ; then
    DATA_FILES=$1
else
    DATA_FILES="/afs/cern.ch/work/v/vhbbframework/public/data/dataForCxAODFramework_190601"
fi
echo "Copying data files from" $DATA_FILES

# check if afs is visible - if not need to scp directory from lxplus using afs username
if [ ! -d $DATA_FILES ] ; then
    echo "The afs directory is not visible (maybe you need a kinit?)"
    echo "We will use CERN_USER to set the afs username and copy using scp" 
    if [ -n "$CERN_USER" ]; then
	echo "CERN_USER is set to:" $CERN_USER
    else
	echo "Enter your CERN_USER name for scp:"
	read -p "CERN_USERname: " CERN_USER
	echo "CERN_USER is now set to" $CERN_USER "use unset CERN_USER if entered incorrectly"
    fi
    target=/tmp/$USER/
    echo "scp from CERN and copy locally to $target"
    COMMAND="scp -pr $CERN_USER@lxplus.cern.ch:$DATA_FILES $target"
    $COMMAND
    DATA_FILES=$target$(basename $DATA_FILES)
fi

# 2. GitLab rules do not allow to store binaries
# so *.root had been removed from data folder
# and are copied now from /afs to data folder
packages=()
# put remote (or /tmp) directories into an array
echo "Reading $DATA_FILES/"
for dir in $DATA_FILES/* ; do 
  if [[ -d "$dir" ]]; then
#      echo 'directory' $dir 
      packages+=($dir)
      bname=$(basename $dir)
      echo "Found data for package: $bname"
  fi
done


echo "Copying data to the corresponding repo packages"
copycommand="cp -Lrp "
for i in "${packages[@]}"
do
    echo "package:$i"
    bname=$(basename $i)
    echo "$bname"
    # look for directory/submodule i.e. depth 2
    found=`find . -maxdepth 1 -mindepth 1 -name ${bname}`
    # if the remote package exists locally copy the file
    # if [[ -d "$bname" ]]; then
    if [[ -d "$found" ]]; then
	echo "found repo directory: $found"
	COMMAND=$copycommand
	COMMAND+="${i}/* ${found}/data/."
	#echo "COMMAND=${COMMAND}"
	eval ${COMMAND}
    fi
done

echo "Copying done"
echo "Note: To set the correct symlinks in ../build/x*/data/ you need the following build sequence:" 
echo 'cd $TestArea; cmake ../<source>; cmake --build .' 
