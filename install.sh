#!/bin/bash

# Copy root files from AFS
source ./copydatafromafs.sh

# Now compile the code
source ./setup.sh

mkdir build run
cd build

cmake .. -DCMAKE_BUILD_TYPE=Release
make
source ${AnalysisBase_PLATFORM}/setup.sh
cd ..
